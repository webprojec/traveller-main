import React, {MouseEventHandler} from 'react'
import type { FC } from 'react'
import { Container, InputRightElement, Input, Heading, InputGroup, IconButton, VStack, UnorderedList, ListItem, Flex } from '@chakra-ui/react'
import { Search2Icon, CloseIcon, CheckIcon } from '@chakra-ui/icons'
import { Box, Table, Thead, Tbody, Tfoot, Tr, Th, Td, TableCaption } from '@chakra-ui/react'


let listCities = [];
let nameHolder = '';
const borderColor = "royalblue";
const boldTextColor = "limegreen";
const bgColorTable = "lightsteelblue";
const bgColorHover = "lightgray";
const lineHeight = "40px";
var ReactDOM = require('react-dom');

const onKeyDownAction = () => {
  if (event && event.target.value.length > 2) {
    var inputString = event.target.value.toLowerCase();
    //get all cities
    fetch('http://localhost:4000/rest/cities').then(function(response) {
      return response.json();
    }).then(function(data){
      //translate city names to lower case letters and search if names contain a match with 'inputString'
      var cityObjects = data.cities.filter(city => city.name.toLowerCase().includes(inputString));

      renderList(cityObjects, inputString);
    }).catch(function() {
      console.log("Booo");
    });
  }
}
function renderList(cities, inputString: string) {
  // listCities = cities.map((city) =>
  //   <Container width="30%"
  //              alignSelf="start"
  //              border="1px solid #527cd1"
  //              borderRadius="5"
  //              mb="5px"
  //              _hover={{
  //                background: "#527cd1",
  //                color: "white",
  //              }}
  //              onClick={() => {
  //                document.getElementById('input').value = city.name;
  //                nameHolder = city.name;
  //
  //              }}
  //              onMouseOver={() => {
  //                nameHolder = document.getElementById('input').value;
  //                document.getElementById('input').value = city.name;
  //              }}
  //              onMouseLeave={() => {document.getElementById('input').value = nameHolder;}}
  //              key={city.id}>{prepareText(city.name, inputString)}
  //   </Container>
  // );
  //ReactDOM.render(<Flex direction="column">{listCities}</Flex>, document.getElementById('aim'));
  listCities = cities.map((city, index) =>
    <Tr
      key={city.id+"row"}
      h={lineHeight}
      _hover={{bgColor:bgColorHover, cursor:"pointer"}}
      borderBottom="1px solid"
      borderColor={borderColor}
      onClick={() => {
        document.getElementById('input').value = city.name;
        nameHolder = city.name;

      }}
      onMouseOver={() => {
        document.getElementById('input').value = city.name;
      }}
      onMouseLeave={() => {document.getElementById('input').value = nameHolder;}}
      >
      <Td key={city.id+"-city"}>{prepareText(city.name, inputString)}</Td>
      <Td key={city.id+"-country"}>{city.country}</Td>
      <Td key={city.id+"-visited"}>{selectIcon(city.visited)}</Td>
      <Td key={city.id+"-wishlist"}>{selectIcon(city.wishlist)}</Td>
    </Tr>
  );

  ReactDOM.render(
    <Box>
      <Table variant='simple' width="100%">
        <Thead>
          <Tr bgColor={bgColorTable} h={lineHeight}>
            <Th>City</Th>
            <Th>Country</Th>
            <Th>Visited</Th>
            <Th>Wishlist</Th>
          </Tr>
        </Thead>
        <Tbody>
          {listCities}
        </Tbody>
      </Table>
    </Box>,
    document.getElementById('aim'));
}

function prepareText(cityName, shouldBeBold ) {
  //'normalSplit' is to check if 'shouldBeBold' is in the beginnig of the cityName
  const normalSplit = cityName.split(shouldBeBold);
  //'twoWordNameSplit' is to check if 'cityName' consists of two words and if 'shouldBeBold' is at the beginnig of 2nd one
  const twoWordNameSplit = cityName.split(shouldBeBold.charAt(0).toUpperCase() + shouldBeBold.slice(1));

  //if 'cityName' begins with 'shouldBeBold'
  if(cityName.toLowerCase().split(shouldBeBold)[0] === "") {
    //make 'shouldBeBold' bold and change 1st letter to upper case
    return boldWithUpper(cityName.toLowerCase().split(shouldBeBold)[1], shouldBeBold);
  } else {
    //if the second word start with 'shouldBeBold'
    if(twoWordNameSplit.length == 2) {
      //leave the first word/part as it is
      //then call 'boldWithUpper'
      return (
         <>
          {twoWordNameSplit[0]}
          {boldWithUpper(twoWordNameSplit[1], shouldBeBold)}
         </>);
    } else {
      //if 'shouldBeBold' is somewhere in the middle
      return (
        <span>
          {normalSplit.map((item, index) => (
            <>
              {item}
              {index !== normalSplit.length - 1 && (
                  <b style={{color: boldTextColor}}>{shouldBeBold}</b>

              )}
            </>
          ))}
        </span>);
    }
  }
}

function boldWithUpper(leaveAsItIs, shouldBeBold) {
  return (
      <>
        <b style={{color: boldTextColor}}>{shouldBeBold.charAt(0).toUpperCase() + shouldBeBold.slice(1)}</b>
        {leaveAsItIs}
      </>
  );
}

function selectIcon(boolVal) {
  console.log(boolVal);
  var icon;
  if(boolVal) {
    icon = (<CheckIcon />);
  } else {
    icon = (<CloseIcon />);
  }
  return (
    <IconButton
    aria-label=""
    icon={icon} />
  );

}


export const Home: FC = () => (
  <VStack spacing="8">
    <Heading as="h1">Smart traveller</Heading>
    <Container maxW="container.md">
      <InputGroup>
        <Input onKeyUp={onKeyDownAction} id="input"/>
        <InputRightElement children={<IconButton aria-label="" icon={<Search2Icon />} />} />
      </InputGroup>
    </Container>
    <Container maxW="container.md" id='aim'></Container>
  </VStack>
)
